""" Provides a lookup table between OpenAI API IDs and their max_tokens values.

Apparently the /models endpoint does not return the max count.
"""

models = [
    {"id": "gpt-3.5-turbo", "max_tokens": 4096},
    {"id": "gpt-3.5-turbo-0301", "max_tokens": 4096},
    {"id": "gpt-3.5-turbo-0613", "max_tokens": 4096},
    {"id": "gpt-3.5-turbo-1106", "max_tokens": 16385},
    {"id": "gpt-3.5-turbo-16k", "max_tokens": 16385},
    {"id": "gpt-3.5-turbo-16k-0613", "max_tokens": 16385},
    {"id": "gpt-3.5-turbo-instruct", "max_tokens": 4096},
    {"id": "gpt-3.5-turbo-instruct-0914", "max_tokens": 4096},
    {"id": "gpt-4", "max_tokens": 8192},
    {"id": "gpt-4-0613", "max_tokens": 8192},
    {"id": "gpt-4-1106-preview", "max_tokens": 128000},
    {"id": "gpt-4-vision-preview", "max_tokens": 128000},
]

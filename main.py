# Copyright (C) 2023-2024, Filippo Alimonda (Literally Useful LLC)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import logging
import signal
import sys

import openai
from PyQt5 import QtCore, QtWidgets

from voxos import Voxos
from voxos.services.agents import AgentService
from voxos.ui.context_menu import ContextMenu
from voxos.ui.hud import HeadsUpDisplay
from voxos.ui.system_tray import SystemTrayIcon
from voxos.ui.text_editor import TextEditor
from voxos.utils.audio_recorder import AudioRecorder
from voxos.utils.constants import Constants
from voxos.utils.cursor import Cursor
from voxos.utils.hotkeys import Hotkeys
from voxos.utils.prompts import Prompts
from voxos.utils.volume import VolumeController
from voxos.version import __version__


def main():
    """This is the main application function that launches Voxos."""

    print(f"\nStarting {Constants.APP_NAME} (v{__version__})...\n")

    funding_url = "https://www.gofundme.com/f/voxos-ai-beta-release"
    print(f"Please consider supporting the project by donating at {funding_url}\n")

    parser = argparse.ArgumentParser(description=f"{Constants.APP_NAME} Application")

    parser.add_argument(
        "--verbose",
        help="Whether or not to enable verbose logging (default: False)",
        action="store_true",
    )

    parser.add_argument(
        "--log_level",
        help="The log level to use, The default without verbose is WARNING, otherwise INFO (default: WARNING)",
        type=str,
        default="WARNING",
    )

    parser.add_argument(
        "--openai_api_key",
        help="OpenAI API Key",
        type=str,
        default="",
    )

    parser.add_argument(
        "--taskbar_notifications",
        help="Whether or not to show taskbar notifications (default: True)",
        type=bool,
        default=True,
    )

    parser.add_argument(
        "--recording_hotkey",
        help="The hotkey to use to toggle recording (default: ctrl+alt+z)",
        type=str,
        default="ctrl+alt+z",
    )

    parser.add_argument(
        "--no_mute",
        help="Whether not to mute the system volume during recording (default: True)",
        action="store_true",
    )

    parser.add_argument(
        "--editor",
        help="The text editor to use when opening new text files (default: notepad)",
        type=str,
        default="notepad",
    )

    args = parser.parse_args()

    level = logging.INFO
    if args.log_level:
        level = logging.getLevelName(args.log_level)
    logging.basicConfig(level=logging.WARNING if not args.verbose else level)
    logger = logging.getLogger(__name__)

    if not args.openai_api_key:
        logger.error("OpenAI key is required")
        parser.print_help()
        sys.exit(1)
    openai.api_key = args.openai_api_key

    # Initialize Qt Application
    app = QtWidgets.QApplication(sys.argv)
    QtCore.QCoreApplication.setApplicationName(Constants.APP_NAME)
    w = QtWidgets.QWidget()

    def handle_interrupt_signal(signum, frame):
        print("Received interrupt signal. Exiting...")
        sys.exit(0)

    signal.signal(signal.SIGINT, handle_interrupt_signal)

    # Initialize Services
    agent_service = AgentService(
        model=Constants.VOXOS_AGENT_MODEL,
        system_prompt=Prompts.VOXOS_AGENT_SYSTEM_PROMPT,
        name=Constants.VOXOS_AGENT_NAME,
        response_detail_level="Normal",
        memory=True,
    )

    # Initialize Utilities
    audio_recorder = AudioRecorder()
    volume_controller = VolumeController(no_mute=args.no_mute)

    # Initialize UI Components
    tray_icon = SystemTrayIcon(w, args.taskbar_notifications)
    hud = HeadsUpDisplay()
    cursor = Cursor()
    context_menu = ContextMenu()
    TextEditor().set_preferred_editor(args.editor)

    # Initialize Hotkeys
    hotkeys = Hotkeys()
    hotkeys.set_recording_hotkey(args.recording_hotkey)

    # Initialize Voxos
    voxos = Voxos(
        audio_recorder=audio_recorder,
        system_tray=tray_icon,
        hotkeys=hotkeys,
        agent_service=agent_service,
        hud=hud,
        cursor=cursor,
        context_menu=context_menu,
        volume_controller=volume_controller,
    )
    # NOTE: this must be called after Voxos is initialized such that methods of Voxos can be properly
    #   used as slots for signals emitted by services
    voxos.setup_signals()

    # Use a timer to periodically yield control to the Python interpreter to catch SIGINT / CTRL + C
    timer = QtCore.QTimer()
    timer.start(500)
    timer.timeout.connect(lambda: None, QtCore.Qt.DirectConnection)

    # Setup application and event loop
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()

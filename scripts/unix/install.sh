#!/bin/bash
#
# install.sh - Installs Voxos in a unix environment
# 
# The script takes the following actions:
# 
# 1. It attempts to detect the Python version installed on the system. 
#    - If a specific Python version is mentioned in the .env file using the VOXOS_PYTHON_VERSION variable, the script will use that version. 
#      If that version is not installed on the system, the script will output an error message and stop execution.
#    - If no version is indicated in .env, it assumes the system default Python3 version.
# 
# 2. The script verifies that the Python version is in the range of 3.8 to 3.12. If it is out of this range, 
#    an error message is printed and the script stops execution.
# 
# 3. It checks for the existence of a hidden virtual environment named .venv or venv, preferring the first if both exist. If no virtual 
#    environment is detected, one is created using the version of Python that was determined in the previous steps. 
#    
# 4. The virtual environment is activated and dependencies are installed from a requirements.txt file located 
#    in the parent directory of the .venv or venv.
# 
# Please note that this script needs to be run from parent directory of the virtual environment and
# it assumes a directory structure where the .env file and the requirements.txt file are in parent directory of 
# venv or .venv folder, and the script itself is in a nested directory. 

set -e

SCRIPTS_DIR="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"

. $SCRIPTS_DIR/utils.sh

echo "Checking Python version..."

PREFERRED_VERSION=$(check_sources 'VOXOS_PYTHON_VERSION')
PYTHON_COMMAND=$(get_python_command $PREFERRED_VERSION)
if [ -z "$PYTHON_COMMAND" ]; then
    echo "Python not found. If you're sure you have a compatible version of Python installed, please consider opening an issue at https://gitlab.com/literally-useful/voxos/-/issues/new"
    exit 1
fi

PYTHON_VERSION=$($PYTHON_COMMAND --version | cut -d' ' -f2)
PYTHON_MAJOR=$(echo $PYTHON_VERSION | cut -d. -f1)
PYTHON_MINOR=$(echo $PYTHON_VERSION | cut -d. -f2)
echo "Using Python version $PYTHON_VERSION, if you'd like to use a different version that's already installed, indicate that version in .env using VOXOS_PYTHON_VERSION."
sleep 1

VENV_DIR=$(find_virtual_env)
if [ -z "$VENV_DIR" ]; then
    echo "No virtual environment found, creating one..."
    $PYTHON_COMMAND -m venv $SCRIPTS_DIR/../../.venv
    VENV_DIR=$(find_virtual_env)
fi

. $SCRIPTS_DIR/../../$VENV_DIR/activate
$PYTHON_COMMAND -m pip install -r $SCRIPTS_DIR/../../requirements.txt

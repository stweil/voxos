#!/bin/bash
#
# run.sh - Runs Voxos in a unix environment
# 
# 1. It starts by checking for the existence a virtual environment creted by the install script.
# 2. The script then checks for the OpenAI API key in the following order of priority:
# 
#     - Command line argument via -o flag
#     - Environment variable VOXOS_OPENAI_API_KEY
#     - Defined in a .env file
# 
# 3. The OpenAI API key is validated against the OpenAI API with a call to the models endpoint.
# 4. The script then sources the virtual environment and activates it. It checks the Python version running in the activated environment to ensure it's Python 3.8 to 3.12. 
#
# Usage:
#     $0 -t -v -o openai_api_key
# 
# Args:
#     -v  Run in verbose mode if set, will print detailed outputs.
#     -o  Provide the OpenAI API key to use.
#
set -e

SCRIPTS_DIR="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"
PROJECT_ROOT="$SCRIPTS_DIR/../.."

. $SCRIPTS_DIR/utils.sh

# Check for existing virtual environment
declare -a venv_paths=(".venv/Scripts" ".venv/bin" "venv/Scripts" "venv/bin")
VENV_DIR=""
for path in "${venv_paths[@]}"; do
  if [ -d "$PROJECT_ROOT/$path" ]; then
    VENV_DIR="$path"
    break
  fi
done

if [ -z "$VENV_DIR" ]; then
  echo "No virtual environment found. Please run install.sh first."
  exit 1
fi

PREFERRED_VERSION=$(check_sources 'VOXOS_PYTHON_VERSION')
PYTHON_COMMAND=$(get_python_command $PREFERRED_VERSION)
if [ -z "$PYTHON_COMMAND" ]; then
    echo "Python not found. If you're sure you have a compatible version of Python installed, please consider opening an issue at https://gitlab.com/literally-useful/voxos/-/issues/new"
    exit 1
fi

verbose=0
openai_api_key=""
recording_hotkey=""
mute_system_volume=""

function check_api_key() {
    response=$(curl -s https://api.openai.com/v1/models -H "Authorization: Bearer ${1}")
    if  [[ "$response" == *"error"* ]]; then
        echo "The provided OpenAI API key is invalid."
        exit 1
    fi
}

while getopts "vorme:" opt; do
  case "$opt" in
  v)  verbose=1
      ;;
  o)  openai_api_key=$OPTARG
      ;;
  r)  recording_hotkey=$OPTARG
      ;;
  m)  mute_system_volume=$OPTARG
      ;;
  e)  editor=$OPTARG
      ;;
  *)  echo "Usage: $0 [-v] [-o openai_api_key] [-r recording_hotkey] [-m mute_bool] [-e editor]" >&2
      exit 1
      ;;
  esac
done

shift $((OPTIND-1))

if [ $verbose -eq 1 ]; then
  VOXOS_VERBOSE_ARGS="--verbose"
fi

## Configuration variable override checks

# OpenAI API key
PROVIDED_KEY=$(check_sources 'VOXOS_OPENAI_API_KEY' "$openai_api_key")
if [ -z "$PROVIDED_KEY" ]; then
  echo "OpenAI API key not found. Please set it in the environment variable VOXOS_OPENAI_API_KEY, pass it as an argument to this script, or define it in a .env file"
  exit 1
fi
check_api_key $PROVIDED_KEY

# Recording hotkey
RECORDING_HOTKEY=$(check_sources 'VOXOS_RECORDING_HOTKEY' "$recording_hotkey")
if [ ! -z "$RECORDING_HOTKEY" ]; then
    RECORDING_HOTKEY_ARGS="--recording_hotkey $RECORDING_HOTKEY"
fi

# Mute system volume
MUTE_SYSTEM_VOLUME=$(check_sources 'VOXOS_MUTE_SYSTEM_VOLUME' "$mute_system_volume")
if [ ! -z "$MUTE_SYSTEM_VOLUME" ] && [ "$MUTE_SYSTEM_VOLUME" -eq 0 ]; then
    MUTE_SYSTEM_VOLUME_ARGS="--no_mute"
fi

# Preferred editor when displaying new text files
EDITOR=$(check_sources 'VOXOS_EDITOR' "$editor")
if [ ! -z "$EDITOR" ]; then
    EDITOR_ARGS="--editor $EDITOR"
fi

## Run Voxos
echo "Sourcing virtual environment"
. "$PROJECT_ROOT/$VENV_DIR/activate"
cd "$PROJECT_ROOT"

version=$($PYTHON_COMMAND -V 2>&1 | grep -Po '(?<=Python )(.+)')
major_version=$(echo "$version" | cut -d. -f1)
minor_version=$(echo "$version" | cut -d. -f2)

# Source .env for additional arguments - TODO @Falimonda - refactor this to use the same check_sources
. $PROJECT_ROOT/.env

if [ ! -z "$VOXOS_VERBOSE" ] && [ "$VOXOS_VERBOSE" -eq 1 ]; then
  echo "Running in verbose mode"
  VERBOSE_ARGS="--verbose"
fi

if [ ! -z "$VOXOS_LOG_LEVEL" ]; then
  echo "Setting log level to $VOXOS_LOG_LEVEL"
  VERBOSE_ARGS="$VERBOSE_ARGS --log_level $VOXOS_LOG_LEVEL"
fi

# Verify that the Python version is supported
if [[ "$major_version" -eq 3 ]] && [[ "$minor_version" -ge 8 ]] && [[ "$minor_version" -le 12 ]]; then
    echo "Running Python $version"
    $PYTHON_COMMAND -m main --openai_api_key $PROVIDED_KEY \
        $RECORDING_HOTKEY_ARGS \
        $MUTE_SYSTEM_VOLUME_ARGS \
        $VERBOSE_ARGS \
        $EDITOR_ARGS
else
    echo "Python version in the activated venv is not supported. Versions 3.8 to 3.12 are supported."
fi

cd -

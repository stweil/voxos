@echo off
setlocal

set SCRIPTS_DIR=%~dp0
set PROJECT_ROOT=%SCRIPTS_DIR%\..\..

:check_sources
setlocal
set env_key_name=%~1
set key=%~2
set result=

if not "%key%"=="" (
    set result=%key%
) else (
    for /f "tokens=*" %%a in ('echo %env_key_name%') do (
        set result=%%a
    )
    if "%key%"=="" (
        if exist .\.env (
            call %PROJECT_ROOT%\.env
            for /f "tokens=*" %%a in ('echo %env_key_name%') do (
                set result=%%a
            )
        )
    )
)
endlocal & set result=%result%
echo %result%
goto :eof
